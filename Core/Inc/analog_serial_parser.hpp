/*
 * analog_serial_parser.hpp
 *
 *  Created on: Mar 18, 2021
 *      Author: kaczp
 */

#ifndef INC_ANALOG_SERIAL_PARSER_HPP_
#define INC_ANALOG_SERIAL_PARSER_HPP_

#include <stm32f4xx_hal.h>

#define ACTIVE_CHANNELS 8

class AnalogSerialParser {
	ADC_HandleTypeDef *hadc;
	TIM_HandleTypeDef *htim;
	UART_HandleTypeDef *huart;
	GPIO_TypeDef *gpio_trigger_port;
	uint16_t gpio_trigger_pin;
	uint8_t tx_buffer[100];
	bool sampling;
	bool data_ready_to_flush;
	bool pending_for_conversion_start;
	bool pending_for_conversion_end;
public:
	typedef struct {
		ADC_HandleTypeDef *hadc;
		TIM_HandleTypeDef *htim;
		UART_HandleTypeDef *huart;
		GPIO_TypeDef *gpio_trigger_port;
		uint16_t gpio_trigger_pin;
	} InitFields;
	uint16_t samples[ACTIVE_CHANNELS];
	AnalogSerialParser() = default;
	AnalogSerialParser(InitFields fields);
	~AnalogSerialParser();
	ADC_HandleTypeDef* getAdcHandler();
	uint16_t getTriggerPin();
	void adcStartSampling();
	void adcStopSampling();
	void adcSetSamplingFrequency(uint32_t frequency);
	void serialFlushSamples();
	void serialTransmitSamples();
	void serialClearBuffer();
	void parseSamplesToCSV();
	void endOfConversionCallback();
	void triggerCallback();
};



#endif /* INC_ANALOG_SERIAL_PARSER_HPP_ */
