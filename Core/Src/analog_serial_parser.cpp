/*
 * analog_serial_parser.cpp
 *
 *  Created on: Mar 18, 2021
 *      Author: kaczp
 */

#include <analog_serial_parser.hpp>
#include <string.h>
#include <sstream>

AnalogSerialParser::AnalogSerialParser(InitFields fields) {
	hadc=fields.hadc;
	htim=fields.htim;
	huart=fields.huart;
	gpio_trigger_port=fields.gpio_trigger_port;
	gpio_trigger_pin=fields.gpio_trigger_pin;
	sampling=false;
	pending_for_conversion_start=false;
	pending_for_conversion_end=false;
	data_ready_to_flush=false;
}
AnalogSerialParser::~AnalogSerialParser() {}
ADC_HandleTypeDef* AnalogSerialParser::getAdcHandler() {
	return hadc;
}
uint16_t AnalogSerialParser::getTriggerPin() {
	return gpio_trigger_pin;
}
void AnalogSerialParser::adcStartSampling() {
	if(!sampling) {
		sampling = true;
		HAL_ADC_Start_DMA(hadc, reinterpret_cast<uint32_t*>(samples), sizeof(samples));
		HAL_TIM_Base_Start(htim);
	}
}
void AnalogSerialParser::adcStopSampling() {
	if(sampling) {
		sampling = false;
		HAL_ADC_Stop_DMA(hadc);
		HAL_TIM_Base_Stop(htim);
	}
}
void AnalogSerialParser::adcSetSamplingFrequency(uint32_t frequency) {
	__HAL_TIM_SET_AUTORELOAD(htim, frequency-1);
}
void AnalogSerialParser::serialFlushSamples() {
	if(sampling) {
		if(data_ready_to_flush) {
			while(HAL_UART_GetState(huart)!=HAL_UART_STATE_READY) {
				HAL_Delay(1);
			}
			serialClearBuffer();
			parseSamplesToCSV();
			serialTransmitSamples();
			data_ready_to_flush = false;
		}
		if(pending_for_conversion_end) {
			pending_for_conversion_end = false;
			HAL_Delay(10);
			if(HAL_GPIO_ReadPin(gpio_trigger_port, gpio_trigger_pin) == GPIO_PIN_RESET) {
				adcStopSampling();
				HAL_Delay(10);
				serialClearBuffer();
				sprintf(reinterpret_cast<char*>(tx_buffer), "END OF CONVERSION\n\r");
				serialTransmitSamples();
			}
		}
	} else if(pending_for_conversion_start) {
		pending_for_conversion_start=false;
		HAL_Delay(10);
		if(HAL_GPIO_ReadPin(gpio_trigger_port, gpio_trigger_pin) == GPIO_PIN_RESET) {
			serialClearBuffer();
			sprintf(reinterpret_cast<char*>(tx_buffer), "START OF CONVERSION\n\r");
			serialTransmitSamples();
			adcStartSampling();
		}
	} else {
		HAL_Delay(1000);
		HAL_SuspendTick();
		HAL_PWR_EnterSLEEPMode(PWR_LOWPOWERREGULATOR_ON, PWR_SLEEPENTRY_WFI);
	}
}
void AnalogSerialParser::serialTransmitSamples() {
	HAL_UART_Transmit_DMA(huart, tx_buffer, static_cast<uint16_t>(strlen(reinterpret_cast<char*>(tx_buffer))));
}

void AnalogSerialParser::serialClearBuffer() {
	memset(tx_buffer,0,sizeof(tx_buffer));
}

void AnalogSerialParser::parseSamplesToCSV() {
	std::stringstream ss;
	for(uint8_t i=0; i<ACTIVE_CHANNELS; i++) {
		ss << samples[i] << ';';
	}
	ss<<'\r'<<'\n';
	memcpy(tx_buffer,ss.str().c_str(),ss.str().length());
}

void AnalogSerialParser::endOfConversionCallback() {
	data_ready_to_flush = true;
}
void AnalogSerialParser::triggerCallback() {
	if(!sampling) {
		HAL_ResumeTick();
		pending_for_conversion_start = true;
	} else {
		pending_for_conversion_end = true;
	}
}
